package titanic;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.parquet.tools.read.SimpleRecord;
import org.apache.parquet.tools.read.SimpleRecord.NameValue;

import java.io.IOException;
import java.util.Objects;

public class TitanicParquetMapper extends Mapper<Void, SimpleRecord, Text, IntWritable> {

  private static final int TRANSPORT_FIELD_INDEX = 5;

  @Override
  public void run(Context context) throws IOException, InterruptedException {

    setup(context);
    try {
      while (context.nextKeyValue()) {

        // Void key = context.getCurrentKey();
        SimpleRecord value = context.getCurrentValue();
        map(null, value, context);
      }
    } catch (Throwable t) {
      t.printStackTrace();
    } finally {
      cleanup(context);
    }
  }

  public void map(Void key, SimpleRecord value, Context context)
      throws IOException, InterruptedException {

    NameValue tuple = value.getValues().get(TRANSPORT_FIELD_INDEX);

    if ("transport".equals(tuple.getName())) {
      context.write(
          new Text(Objects.toString(tuple.getValue(), "NULL")),
          new IntWritable(1));
    }
  }
}
