
package titanic;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.parquet.hadoop.ParquetInputFormat;
import org.apache.parquet.tools.read.SimpleReadSupport;

import java.io.IOException;

public class TitanicParquetDriver extends Configured implements Tool {

  public static void main(String args[]) throws Exception {
    int res = ToolRunner.run(new TitanicParquetDriver(), args);
    System.exit(res);
  }

  public int run(String[] args) throws Exception {

    Configuration conf = getConf();

    final Path inputPath = new Path(
        "s3a://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ng_event_store/default/shodan/data/ev_date=2023-01-12/*/*.parquet");

    final Path outputPath = new Path(
        "s3a://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/parquet_out/");



    deleteTarget(conf, outputPath);

    JobConf jobConf = new JobConf(conf);
    Job job = Job.getInstance(jobConf, "titanic-parquet");
    
    ParquetInputFormat.setInputPaths(job, inputPath);
    ParquetInputFormat.setReadSupportClass(job, SimpleReadSupport.class);
    //ParquetInputFormat.setFilterPredicate(conf, null)

    //FileInputFormat.setInputPaths(job, inputPath);
    FileOutputFormat.setOutputPath(job, outputPath);

    job.setJobName("titanic-parquet");
    job.setJarByClass(TitanicParquetDriver.class);


    job.setInputFormatClass(ParquetInputFormat.class);
    job.setOutputFormatClass(TextOutputFormat.class);
    job.setMapOutputKeyClass(Text.class);
    job.setMapOutputValueClass(IntWritable.class);

    job.setMapperClass(TitanicParquetMapper.class);
    job.setReducerClass(IcebergPoCReducer.class);

    int exitStatus = job.waitForCompletion(true) ? 0 : 1;
    return exitStatus;
  }

  private void deleteTarget(Configuration conf, final Path outputPath) throws IOException {
    FileSystem fs = outputPath.getFileSystem(conf);

    // delete existing directory
    if (fs.exists(outputPath)) {
      fs.delete(outputPath, true);
    }
  }
}
