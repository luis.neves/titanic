package titanic;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.regex.Pattern;

public class DummyMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
  
  private final static IntWritable one = new IntWritable(1);
  private static final Pattern splitter = Pattern.compile("\\W+");
  private Text word = new Text();

  @Override
  public void map(LongWritable key, Text value, Context context)
      throws IOException, InterruptedException {
    String line = value.toString().toLowerCase();

    String[] tokens = splitter.split(line);
    for (String token : tokens) {
      word.set(token);
      context.write(word, one);
    }
  }
}
