package titanic;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.iceberg.data.Record;

import java.io.IOException;
import java.util.Objects;

public class IcebergPocMapper extends Mapper<Void, Record, Text, IntWritable> {

  @Override
  protected void cleanup(Mapper<Void, Record, Text, IntWritable>.Context context)
      throws IOException, InterruptedException {
    super.cleanup(context);
  }

  @Override
  public void run(Context context) throws IOException, InterruptedException {

    setup(context);
    try {
      while (context.nextKeyValue()) {

        Void key = context.getCurrentKey();
        Record value = context.getCurrentValue();
        map(key, value, context);
      }
    } catch (Throwable t) {
      t.printStackTrace();
    } finally {
      cleanup(context);
    }
  }


  @Override
  protected void map(Void key, Record value, Context context) throws IOException,
      InterruptedException {

    context.write(
        new Text(Objects.toString(value.getField("transport"), "NULL")),
        new IntWritable(1));
    // context.write(
    // new Text(value.getField("version").toString()),
    // new IntWritable(1)
    // );
    // context.write(
    // new Text(value.getField("timestamp").toString()),
    // new IntWritable(1)
    // );
    // context.write(
    // new Text(value.getField("info").toString()),
    // new IntWritable(1)
    // );
    // context.write(
    // new Text(value.toString()),
    // new IntWritable(1)
    // );
  }

}
