package titanic;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class DummyDriver extends Configured implements Tool {

  public static void main(String args[]) throws Exception {
    int res = ToolRunner.run(new DummyDriver(), args);
    System.exit(res);
  }

  public int run(String[] args) throws Exception {

    if (args.length != 2) {
      System.err.println("Usage: Dummy <input path> <output path>");
      System.exit(0);
    }
    Path inputPath = new Path(args[0]);
    Path outputPath = new Path(args[1]);

    if (args.length != 2) {
      System.err.println("Usage: Dummy <input path> <output path>");
      System.exit(0);
    }

    Configuration conf = getConf();

    FileSystem fs = FileSystem.get(conf);

    // delete existing directory
    if (fs.exists(outputPath)) {
      fs.delete(outputPath, true);
    }

    Job job = Job.getInstance(conf, "dummy");

    FileInputFormat.setInputPaths(job, inputPath);
    FileOutputFormat.setOutputPath(job, outputPath);

    job.setJobName("DummyDriver");
    job.setJarByClass(DummyDriver.class);
    job.setInputFormatClass(TextInputFormat.class);
    job.setOutputFormatClass(TextOutputFormat.class);
    job.setMapOutputKeyClass(Text.class);
    job.setMapOutputValueClass(IntWritable.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);

    job.setMapperClass(DummyMapper.class);
    job.setCombinerClass(DummyReducer.class);
    job.setReducerClass(DummyReducer.class);

    return job.waitForCompletion(true) ? 0 : 1;
  }
}
