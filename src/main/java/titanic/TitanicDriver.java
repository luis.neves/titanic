
package titanic;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.iceberg.CatalogProperties;
import org.apache.iceberg.SchemaParser;
import org.apache.iceberg.Table;
import org.apache.iceberg.expressions.Expressions;
import org.apache.iceberg.mr.Catalogs;
import org.apache.iceberg.mr.InputFormatConfig;
import org.apache.iceberg.mr.hive.HiveIcebergStorageHandler;
import org.apache.iceberg.mr.mapreduce.IcebergInputFormat;

import java.util.Optional;

public class TitanicDriver extends Configured implements Tool {

  public static void main(String args[]) throws Exception {
    int res = ToolRunner.run(new TitanicDriver(), args);
    System.exit(res);
  }

  public int run(String[] args) throws Exception {

//    Path outputPath = new Path(
//        "s3://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/mr_out/");
//
//    final String warehousePath =
//        "s3://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ng_event_store/";

  Path outputPath = new Path("/Users/luisneves/tmp/wc/out/");

  final String warehousePath = "/Users/luisneves/bs_ng_evstore/dev_db/";

    Configuration conf = getConf();
    conf.set(CatalogProperties.WAREHOUSE_LOCATION, warehousePath);
    conf.set(CatalogProperties.CATALOG_IMPL, "org.apache.iceberg.hadoop.HadoopCatalog");

    conf.set(InputFormatConfig.CATALOG_NAME, "local");
    conf.set(InputFormatConfig.TABLE_IDENTIFIER, "default.shodan");

    conf.set("iceberg.catalog.local.catalog-impl", "org.apache.iceberg.hadoop.HadoopCatalog");
    conf.set("iceberg.catalog.local.warehouse", warehousePath);

    Table table =
        Optional.ofNullable(
            HiveIcebergStorageHandler.table(conf, conf.get(InputFormatConfig.TABLE_IDENTIFIER)))
            .orElseGet(() -> Catalogs.loadTable(conf));

    conf.set(InputFormatConfig.TABLE_SCHEMA, SchemaParser.toJson(table.schema()));


    FileSystem fs = outputPath.getFileSystem(conf);

    // delete existing directory
    if (fs.exists(outputPath)) {
      fs.delete(outputPath, true);
    }

    JobConf jobConf = new JobConf(conf);
    Job job = Job.getInstance(jobConf, "titanic");

    InputFormatConfig.ConfigBuilder confBuilder = IcebergInputFormat.configure(job);

    // confBuilder
    // .filter(
    // Expressions.and(
    // Expressions.equal("ev_date", "2023-01-12"),
    // Expressions.equal("inet_addr", "34.111.159.238")));

    confBuilder
        .filter(Expressions.equal("ev_date", "2023-01-12"));

    FileOutputFormat.setOutputPath(job, outputPath);

    job.setJobName("titanic");
    job.setJarByClass(TitanicDriver.class);

    job.setOutputFormatClass(TextOutputFormat.class);
    job.setMapOutputKeyClass(Text.class);
    job.setMapOutputValueClass(IntWritable.class);

    job.setMapperClass(IcebergPocMapper.class);
    job.setReducerClass(IcebergPoCReducer.class);

    int exitStatus = job.waitForCompletion(true) ? 0 : 1;
    return exitStatus;
  }
}
